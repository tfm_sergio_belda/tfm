using System;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;


namespace Game
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;
        
        private GameColor _nextPlayersColor;

        public int numPlayersTeamKind;
        
        public int numPlayersTeamWicked;

        public PhotonView photonView;

        public event Action OnNumPlayersUpdated;

        private List<GameColor> _currentGameColors;

        public GameMode gameMode;
        
        private void Awake()
        {
            if (Instance != null)
            {
                DestroyImmediate(gameObject);
                return;
            }
            Instance = this;
            PhotonNetwork.AutomaticallySyncScene = true;
        }

        private void Start()
        {
            _nextPlayersColor = GameColor.Yellow;
            _currentGameColors = new List<GameColor>();
            if (!photonView.IsMine) return;
            photonView.RPC("RPC_GetNumPlayers", RpcTarget.MasterClient);
            photonView.RPC("RPC_GetNextPlayersColor", RpcTarget.MasterClient);
        }

        private void Update()
        {
            if (PhotonNetwork.IsMasterClient)
            {
                if (gameMode != GameMode.Playing)
                {
                    return;
                }
                if (numPlayersTeamKind == 0 || numPlayersTeamWicked == 0)
                {
                    if (numPlayersTeamKind == 0)
                    {
                        PhotonNetwork.RaiseEvent(EventCode.TeamWickedWin, null, new RaiseEventOptions { Receivers = ReceiverGroup.All }, SendOptions.SendUnreliable);
                    }

                    if (numPlayersTeamWicked == 0)
                    {
                        PhotonNetwork.RaiseEvent(EventCode.TeamKindWin, null, new RaiseEventOptions { Receivers = ReceiverGroup.All }, SendOptions.SendUnreliable);
                    }
                    EndGame();
                }
            }
        }

        [PunRPC]
        private void RPC_GetNumPlayers()
        {
            photonView.RPC("RPC_SentNumPlayers", RpcTarget.OthersBuffered, numPlayersTeamKind, numPlayersTeamWicked);
        }
        
        [PunRPC]
        private void RPC_GetNextPlayersColor()
        {
            photonView.RPC("RPC_SentNextPlayersColor", RpcTarget.OthersBuffered, _nextPlayersColor);
        }
        
        [PunRPC]
        private void RPC_SentNextPlayersColor(GameColor gameColor)
        {
            _nextPlayersColor = gameColor;
        }

        [PunRPC]
        private void RPC_SentNumPlayers(int numPlayersKind, int numPlayersWicked)
        {
            numPlayersTeamKind = numPlayersKind;
            numPlayersTeamWicked = numPlayersWicked;
        }
        
        [PunRPC]
        private void RPC_UpdateNumTeamKindPlayer(int numPlayersKind)
        {
            numPlayersTeamKind = numPlayersKind;
            OnNumPlayersUpdated?.Invoke();
        }
        
        [PunRPC]
        private void RPC_UpdateNumTeamWickedPlayer(int numPlayersWicked)
        {
            numPlayersTeamWicked = numPlayersWicked;
            OnNumPlayersUpdated?.Invoke();
        }
        
        public void Play()
        {
            gameMode = GameMode.Playing;
            PhotonManager.Instance.ChangeRoomVisibility(false);
            PhotonNetwork.RaiseEvent(EventCode.StartGame, null, new RaiseEventOptions { Receivers = ReceiverGroup.All }, SendOptions.SendUnreliable);
        }

        private void EndGame()
        {
            gameMode = GameMode.End;
            PhotonNetwork.RaiseEvent(EventCode.EndGame, null, new RaiseEventOptions { Receivers = ReceiverGroup.All }, SendOptions.SendUnreliable);
        }

        public GameColor GetColor()
        {
            _currentGameColors.Add(_nextPlayersColor);
            return _nextPlayersColor;
        }

        public void UpdateColor()
        {
            switch (_nextPlayersColor)
            {
                case GameColor.Blue : 
                    _nextPlayersColor = GameColor.Red; 
                    break; 
                case GameColor.Red : 
                    _nextPlayersColor = GameColor.Yellow;
                    break;
                case GameColor.Yellow :
                    _nextPlayersColor = GameColor.Green;
                    break;
                case GameColor.Green :
                    _nextPlayersColor = GameColor.Blue;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            photonView.RPC("RPC_SentNextPlayersColor", RpcTarget.AllBuffered, _nextPlayersColor);
        }

        public void SetTeam(Team team)
        {
            if (team == Team.Kind)
            {
                if (numPlayersTeamKind < 2)
                {
                    numPlayersTeamKind++;
                    photonView.RPC("RPC_UpdateNumTeamKindPlayer", RpcTarget.AllBuffered, numPlayersTeamKind);
                }
            }
            else if (team == Team.Wicked)
            {
                if (numPlayersTeamWicked < 2)
                {
                    numPlayersTeamWicked++;
                    photonView.RPC("RPC_UpdateNumTeamWickedPlayer", RpcTarget.AllBuffered, numPlayersTeamWicked);
                }
            }
        }
        
        public void PlayerLeaveTeam(Team team)
        {
            if (team == Team.Kind)
            {
                numPlayersTeamKind--;
                photonView.RPC("RPC_UpdateNumTeamKindPlayer", RpcTarget.AllBuffered, numPlayersTeamKind);
            } else if (team == Team.Wicked)
            {
                numPlayersTeamWicked--;
                photonView.RPC("RPC_UpdateNumTeamWickedPlayer", RpcTarget.AllBuffered, numPlayersTeamWicked);
            }
        }

        public int GetNumPlayers()
        {
            return numPlayersTeamKind + numPlayersTeamWicked;
        }

        public List<GameColor> GetCurrentGameColors()
        {
            return _currentGameColors;
        }
    }
}