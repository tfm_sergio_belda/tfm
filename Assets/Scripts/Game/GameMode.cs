namespace Game
{
    /// <summary>
    /// Game state
    /// </summary>
    public enum GameMode
    {
        Begin,
        Playing,
        End
    }
}