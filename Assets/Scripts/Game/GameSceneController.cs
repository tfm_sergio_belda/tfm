﻿using GoogleARCore;
using GoogleARCore.Examples.CloudAnchors;
using Photon;
using Photon.Pun;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using Input = GoogleARCore.InstantPreviewInput;
#endif
 
namespace Game
{
    /// <summary>
    /// Scene controller
    /// </summary>
    public class GameSceneController : MonoBehaviour
    {
        private LocalPlayerController _localPlayerController;
    
        /// <summary>
        /// True if the app is in the process of quitting due to an ARCore connection error,
        /// otherwise false.
        /// </summary>
        private bool _mIsQuitting;

        /// <summary>
        /// Indicates whether the Origin of the new World Coordinate System, i.e. the Cloud Anchor,
        /// was placed.
        /// </summary>
        private bool _mIsOriginPlaced;

        /// <summary>
        /// Indicates whether the Cloud Anchor finished hosting.
        /// </summary>
        private bool _mAnchorFinishedHosting;

        /// <summary>
        /// Indicates whether the Anchor was already instantiated.
        /// </summary>
        private bool _mAnchorAlreadyInstantiated;

        /// <summary>
        /// The last pose of the hit point from AR hit test.
        /// </summary>
        private Pose? _mLastHitPose;

        /// <summary>
        /// The anchor component that defines the shared world origin.
        /// </summary>
        private Component _mWorldOriginAnchor;

        /// <summary>
        /// The helper that will calculate the World Origin offset when performing a raycast or
        /// generating planes.
        /// </summary>
        public ARCoreWorldOriginHelper ARCoreWorldOriginHelper;

        /// <summary>
        /// Show messages to the user
        /// </summary>
        public GameUIController _gameUIController;

        public GameObject arCoreDevice;
    
        public GameObject pointCloud;

        /// <summary>
        /// The current cloud anchor mode.
        /// </summary>
        private ApplicationMode _mCurrentMode = ApplicationMode.Ready;

        /// <summary>
        /// Enumerates modes the application can be in.
        /// </summary>
        private enum ApplicationMode
        {
            Ready,
            Hosting,
            Resolving,
        }

        private TrackableHit _arcoreHitResult;

        /// <summary>
        /// 
        /// </summary>
        private GameObject _boardInstance;

        /// <summary>
        /// 
        /// </summary>
        public GameObject boardTrackingPrefab;

        private void Awake()
        {
            gameObject.name = "GameSceneController";
        }
        
        private void Start()
        {
            _localPlayerController = GameObject.Find("LocalPlayer").GetComponent<LocalPlayerController>();
            _localPlayerController.OnTeamSelected += OnTeamSelected;
        }

        void Update()
        {
            _UpdateApplicationLifecycle();

            if (Session.Status == SessionStatus.LostTracking && Session.LostTrackingReason != LostTrackingReason.None)
            {
                _gameUIController.OnLostTracking();
            }

            // If we are neither in hosting nor resolving mode then the update is complete.
            if (_mCurrentMode != ApplicationMode.Hosting && _mCurrentMode != ApplicationMode.Resolving)
            {
                return;
            }

            // If the origin anchor has not been placed yet, then update in resolving mode is
            // complete.
            if (_mCurrentMode == ApplicationMode.Resolving && !_mIsOriginPlaced)
            {
                return;
            }

            // If the player has not touched the screen then the update is complete.
            Touch touch;
            if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began)
            {
                return;
            }

            if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
            {
                return;
            }

            var arcoreHitResult = new TrackableHit();
            _mLastHitPose = null;

            // Raycast against the location the player touched to search for planes.
            if (ARCoreWorldOriginHelper.Raycast(touch.position.x, touch.position.y,
                TrackableHitFlags.PlaneWithinPolygon, out arcoreHitResult))
            {
                _mLastHitPose = arcoreHitResult.Pose;
            }

            // If there was an anchor placed, then instantiate the corresponding object.
            if (_mLastHitPose != null)
            {
                if (!_mIsOriginPlaced && _mCurrentMode == ApplicationMode.Hosting)
                {
                    this._arcoreHitResult = arcoreHitResult;
                    _InstantiateTrackingBoard();
                }
            }
        }

        private void OnTeamSelected(Team team)
        {
            arCoreDevice.SetActive(true);
            pointCloud.SetActive(false);
        
            if (PhotonNetwork.IsMasterClient)
            {
                OnEnterHostingMode();
            }
            else
            {
                OnEnterResolvingMode();
            }
        }

        private void _InstantiateTrackingBoard()
        {
            if (_boardInstance != null)
            {
                DestroyImmediate(_boardInstance);
            }

            if (_mLastHitPose != null)
            {
                Vector3 pos = _mLastHitPose.Value.position;
                Quaternion rotation = Frame.Pose.rotation;
                rotation.z = 0;
                rotation.x = 0;
                _boardInstance = Instantiate(boardTrackingPrefab, pos, rotation, transform);

                _gameUIController.OnTrackingBoardInstantiated();
            }
        }


        /// <summary>
        /// Procede a crear el anchor de nuestro board en sitio donde se había clickado previamente para colocar el board
        /// de posicionamiento o Tracking.
        /// </summary>
        public void AnchorBoard()
        {
            _mWorldOriginAnchor = _arcoreHitResult.Trackable.CreateAnchor(_arcoreHitResult.Pose);
            SetWorldOrigin(_mWorldOriginAnchor.transform);
            _InstantiateAnchor();
            OnAnchorInstantiated(true);

            DestroyImmediate(_boardInstance);
        }

        #region Google Cloud Anchors
        private void _InstantiateAnchor()
        {
            _localPlayerController.SpawnAnchor(Vector3.zero, Quaternion.identity, _mWorldOriginAnchor);
        }

        /// <summary>
        /// Callback indicating that the Cloud Anchor was hosted.
        /// </summary>
        /// <param name="success">If set to <c>true</c> indicates the Cloud Anchor was hosted successfully.</param>
        /// <param name="response">The response string received.</param>
        public void OnAnchorHosted(bool success, string response)
        {
            _gameUIController.OnAnchorHosted(success, response);
        }

        public void OnAnchorResolved(bool success, string response)
        {
            _gameUIController.OnAnchorResolved(success, response);
        }

        /// <summary>
        /// Callback indicating that the Cloud Anchor was instantiated and the host request was
        /// made.
        /// </summary>
        /// <param name="isHost">Indicates whether this player is the host.</param>
        public void OnAnchorInstantiated(bool isHost)
        {
            if (_mAnchorAlreadyInstantiated)
            {
                return;
            }

            _mAnchorAlreadyInstantiated = true;

            _gameUIController.OnAnchorInstantiated(isHost);
        }

        /// <summary>
        /// Sets the apparent world origin so that the Origin of Unity's World Coordinate System
        /// coincides with the Anchor. This function needs to be called once the Cloud Anchor is
        /// either hosted or resolved.
        /// </summary>
        /// <param name="anchorTransform">Transform of the Cloud Anchor.</param>
        public void SetWorldOrigin(Transform anchorTransform)
        {
            if (_mIsOriginPlaced)
            {
                Debug.LogWarning("The World Origin can be set only once.");
                return;
            }

            _mIsOriginPlaced = true;

            ARCoreWorldOriginHelper.SetWorldOrigin(anchorTransform);
        }

        /// <summary>
        /// Handles user intent to enter a mode where they can place an anchor to host or to exit
        /// this mode if already in it.
        /// </summary>
        private void OnEnterHostingMode()
        {
            if (_mCurrentMode == ApplicationMode.Hosting)
            {
                _mCurrentMode = ApplicationMode.Ready;
                _ResetStatus();
            }
        
            _mCurrentMode = ApplicationMode.Hosting;
        
            _gameUIController.OnEnterHostingMode();
        }

        /// <summary>
        /// Handles a user intent to enter a mode where they can input an anchor to be resolved or exit this mode if
        /// already in it.
        /// </summary>
        private void OnEnterResolvingMode()
        {
            if (_mCurrentMode == ApplicationMode.Resolving)
            {
                _mCurrentMode = ApplicationMode.Ready;
                _ResetStatus();
                return;
            }
        
            _mCurrentMode = ApplicationMode.Resolving;
        
            _gameUIController.OnEnterResolvingMode();
        }

        private void _ResetStatus()
        {
            // Reset internal status.
            _mCurrentMode = ApplicationMode.Ready;
            if (_mWorldOriginAnchor != null)
            {
                Destroy(_mWorldOriginAnchor.gameObject);
            }

            _mWorldOriginAnchor = null;
        }

        #endregion

        #region Application Lifecycle

        /// <summary>
        /// Check and update the application lifecycle.
        /// </summary>
        private void _UpdateApplicationLifecycle()
        {
            // Exit the app when the 'back' button is pressed.
            if (UnityEngine.Input.GetKeyDown(KeyCode.Escape))
            {
                _DoQuit();
            }

            var sleepTimeout = SleepTimeout.NeverSleep;

            if (Session.Status != SessionStatus.Tracking)
            {
                const int lostTrackingSleepTimeout = 15;
                sleepTimeout = lostTrackingSleepTimeout;
            }

            Screen.sleepTimeout = sleepTimeout;

            if (_mIsQuitting)
            {
                return;
            }

            // Quit if ARCore was unable to connect and give Unity some time for the toast to appear.
            if (Session.Status == SessionStatus.ErrorPermissionNotGranted)
            {
                _ShowAndroidToastMessage("Camera permission is needed to run this application.");
                _mIsQuitting = true;
                Invoke("_DoQuit", 0.5f);
            }
            else if (Session.Status.IsError())
            {
                _ShowAndroidToastMessage("ARCore encountered a problem connecting.  Please start the app again.");
                _mIsQuitting = true;
                Invoke("_DoQuit", 0.5f);
            }
        }

        /// <summary>
        /// Actually quit the application.
        /// </summary>
        public void _DoQuit()
        {
            _localPlayerController.LeaveMatch();
            PhotonManager.Instance.LeaveRoom();
            SceneManager.LoadScene("Menu");
        }

        private void OnApplicationQuit()
        {
            _localPlayerController.LeaveMatch();
            Debug.Log("Quit");
        }

        /// <summary>
        /// Show an Android toast message.
        /// </summary>
        /// <param name="message">Message string to show in the toast.</param>
        private void _ShowAndroidToastMessage(string message)
        {
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            if (unityActivity != null)
            {
                AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
                unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
                {
                    AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
                        message, 0);
                    toastObject.Call("show");
                }));
            }
        }

        #endregion
    }
}