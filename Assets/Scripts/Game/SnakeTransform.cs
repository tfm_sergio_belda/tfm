﻿﻿﻿using System;
  using Photon.Pun;
  using UnityEngine;
  using Random = UnityEngine.Random;

  namespace Game
{
    public class SnakeTransform : MonoBehaviour, IPunObservable
    {
        public float speed;

        public float xMax;
        public float zMax;
        public float xMin;
        public float zMin;

        private Vector3 _targetPosition;

        public PhotonView _photonView;

        // Start is called before the first frame update
        private void Start()
        {
            _targetPosition = transform.position;
        }

        // Update is called once per frame
        private void Update()
        {
            if (_photonView.IsMine)
            {
                float step =  speed * Time.deltaTime; // calculate distance to move
                transform.position = Vector3.MoveTowards(transform.position, _targetPosition, step);
                transform.LookAt(_targetPosition);
                if (Vector3.Distance(transform.position, _targetPosition) < 0.001f)
                {
                    GetRandomPosition();
                }
            }
            else
            {
                SmoothNetMovement();
            }
        }

        private void SmoothNetMovement()
        {
            float step =  speed * Time.deltaTime; // calculate distance to move
            transform.position = Vector3.MoveTowards(transform.position, _targetPosition, step); 
        }

        private void GetRandomPosition()
        {
            _targetPosition = transform.position;
            _targetPosition.x = Random.Range(xMin, xMax);
            _targetPosition.z = Random.Range(zMin, zMax);
        }

        public void IncreaseSpeed(float amount)
        {
            speed += amount;
        }
        
        public void ReduceSpeed(float amount)
        {
            speed -= amount;
        }

        public void SetXMax(float xMax)
        {
            this.xMax = xMax;
        }

        public void SetZMax(float zMax)
        {
            this.zMax = zMax;
        }

        public void SetXMin(float xMin)
        {
            this.xMin = xMin;
        }

        public void SetZMin(float zMin)
        {
            this.zMin = zMin;
        }

        public void StopSnake()
        {
            speed = 0;
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(transform.position);
                stream.SendNext(speed);
            }
            else
            {
                _targetPosition = (Vector3) stream.ReceiveNext();
                speed = (float) stream.ReceiveNext();
            }
        }
        
    }
}