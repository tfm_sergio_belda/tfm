using System;
using TMPro;
using UnityEngine;

namespace Game
{
    public class ScoreboardController : MonoBehaviour
    {
        private void Awake()
        {
            gameObject.name = "ScoreboardController";
        }

        private void Start()
        {

        }

        private void Update()
        {
            
        }
        
        public void SetScoreText(string text)
        {
            var textMeshPro = transform.GetChild(0).transform.GetChild(0).GetComponent<TextMeshPro>();
            textMeshPro.text = "Score: " + text;
        }
    }
}