using System.Collections;
using ExitGames.Client.Photon;
using Photon.Pun;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Controlador de objetos de comida que aparecen en la escena
    /// </summary>
    public class FoodController : MonoBehaviour
    {
        /// <summary>
        /// GameController
        /// </summary>
        private GameManager _gameManager;
        
        /// <summary>
        /// Tiempo de aparición del objeto
        /// </summary>
        private float _foodAge;

        public GameObject[] foodModels;

        private GameObject _foodInstance;

        private void Awake()
        {
            _gameManager = GameManager.Instance;
        }

        public void OnEnable()
        {
            PhotonNetwork.NetworkingClient.EventReceived += EventReceived;
        }

        public void OnDisable()
        {
            PhotonNetwork.NetworkingClient.EventReceived -= EventReceived;
        }

        private void EventReceived(EventData obj)
        {
            if (obj.Code == EventCode.StartGame)
            {
                StartCoroutine(InstantiateFood());
            }
        }

        private IEnumerator InstantiateFood()
        {
            while (true)
            {
                yield return new WaitForSeconds(4);
                PhotonNetwork.Destroy(_foodInstance);
                _InstantiateFood();
            }
            
        }
        
        private void _InstantiateFood()
        {
            Vector3 position = new Vector3();
            position.x = Random.Range(-0.2f, 0.2f);
            position.y = 0.06f;
            position.z = Random.Range(-0.2f, 0.2f);
            GameObject foodItem = foodModels[Random.Range (0, foodModels.Length)];
            _foodInstance = PhotonNetwork.Instantiate(foodItem.name, position, Quaternion.identity);
            _foodInstance.tag = "food";
        }

        private void Update()
        {
        }
    }
}