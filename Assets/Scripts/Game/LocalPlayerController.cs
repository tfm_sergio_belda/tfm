﻿using CloudAnchor;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Game
{
    public class LocalPlayerController : MonoBehaviourPunCallbacks
    {
        public PhotonView photonView;

        public GameObject anchorPrefab;
        
        private GameObject _scoreInstance;

        public GameColor myColor;

        private bool _colorSelected = false;

        public delegate void ColorSelectedEventHandler(GameColor color);

        public event ColorSelectedEventHandler OnColorSelected;

        public Team myTeam;

        private bool _teamSelected = false;

        public delegate void TeamSelectedEventHandler(Team team);

        public event TeamSelectedEventHandler OnTeamSelected;

        public delegate void ScoreChangeEventHandler(int score);

        public event ScoreChangeEventHandler OnScoreChange;

        public Hashtable playerCustomProperties = new Hashtable();

        private void Awake()
        {
            gameObject.name = "LocalPlayer";
        }

        // Start is called before the first frame update
        private void Start()
        {
            photonView = GetComponent<PhotonView>();
            if (photonView.IsMine)
            {
                photonView.RPC("RPC_GetColor", RpcTarget.MasterClient);
            }

            playerCustomProperties.Add("Score", 100);
            PhotonNetwork.LocalPlayer.SetCustomProperties(playerCustomProperties);
        }

        public override void OnEnable()
        {
            PhotonNetwork.NetworkingClient.EventReceived += EventReceived;
        }

        public override void OnDisable()
        {
            PhotonNetwork.NetworkingClient.EventReceived -= EventReceived;
        }


        private void EventReceived(EventData obj)
        {
            if (obj.Code == EventCode.EndGame)
            {
                LeaveMatch();
            }
            else if (obj.Code == EventCode.Punish)
            {
                if (photonView.IsMine)
                {
                    var data = (object[]) obj.CustomData;
                    var color = (GameColor) data[0];
                    if (myColor == color)
                    {
                        if (myTeam == Team.Wicked)
                        {
                            ReduceScore(20);
                        }
                    }
                }
            }
            else if (obj.Code == EventCode.Pollute)
            {
                if (photonView.IsMine)
                {
                    var data = (object[]) obj.CustomData;
                    var color = (GameColor) data[0];
                    if (myColor == color)
                    {
                        if (myTeam == Team.Kind)
                        {
                            ReduceScore(20);
                        }
                    }
                }
            }
        }

        public void SelectTeam(Team team)
        {
            if (photonView.IsMine)
            {
                photonView.RPC("RPC_SetTeam", RpcTarget.MasterClient, team);
            }
        }

        public void LeaveMatch()
        {
            if (photonView.IsMine)
            {
                photonView.RPC("RPC_PlayerLeaveTeam", RpcTarget.MasterClient, myTeam);
                PhotonNetwork.Destroy(gameObject);
            }
        }

        /// <summary>
        /// Will spawn the origin anchor and host the Cloud Anchor. Must be called by the host.
        /// </summary>
        /// <param name="position">Position of the object to be instantiated.</param>
        /// <param name="rotation">Rotation of the object to be instantiated.</param>
        /// <param name="anchor">The ARCore Anchor to be hosted.</param>
        public void SpawnAnchor(Vector3 position, Quaternion rotation, Component anchor)
        {
            // Instantiate Anchor model at the hit pose.
            var anchorObject = PhotonNetwork.Instantiate(anchorPrefab.name, position, rotation, 0);

            // Anchor must be hosted in the device.
            anchorObject.GetComponent<AnchorController>().HostLastPlacedAnchor(anchor);
        }

        // Update is called once per frame
        private void Update()
        {
            if (_colorSelected == false && myColor != 0)
            {
                if (photonView.IsMine)
                {
                    _colorSelected = true;
                    OnColorSelected?.Invoke(myColor);
                    OnScoreChange?.Invoke(100);
                }
            }

            if (_teamSelected == false && myTeam != 0)
            {
                if (photonView.IsMine)
                {
                    _teamSelected = true;
                    OnTeamSelected?.Invoke(myTeam);
                }
            }

            if ((int) PhotonNetwork.LocalPlayer.CustomProperties["Score"] <= 0)
            {
                if (photonView.IsMine)
                {
                    LeaveMatch();
                }
            }

            var score = (int) PhotonNetwork.LocalPlayer.CustomProperties["Score"];
            OnScoreChange?.Invoke(score);
        }

        [PunRPC]
        private void RPC_GetColor()
        {
            myColor = GameManager.Instance.GetColor();
            GameManager.Instance.UpdateColor();
            photonView.RPC("RPC_SentColor", RpcTarget.OthersBuffered, myColor);
        }

        [PunRPC]
        private void RPC_SetTeam(Team team)
        {
            myTeam = team;
            GameManager.Instance.SetTeam(myTeam);
            photonView.RPC("RPC_SentTeam", RpcTarget.OthersBuffered, myTeam);
        }

        [PunRPC]
        private void RPC_PlayerLeaveTeam(Team team)
        {
            GameManager.Instance.PlayerLeaveTeam(team);
        }

        [PunRPC]
        private void RPC_SentTeam(Team whichTeam)
        {
            myTeam = whichTeam;
            Debug.Log(myTeam);
        }

        [PunRPC]
        private void RPC_SentColor(GameColor whichColor)
        {
            myColor = whichColor;
            Debug.Log(myColor.ToString());
        }

        public override void OnMasterClientSwitched(Player newMasterClient)
        {
            Debug.Log(newMasterClient.UserId);
            Debug.Log(PhotonNetwork.IsMasterClient);
            Debug.Log(PhotonNetwork.LocalPlayer.UserId);
            Debug.Log(PhotonNetwork.CurrentRoom.PlayerCount);
        }

        public void ReduceScore(int amount)
        {
            int score = (int) PhotonNetwork.LocalPlayer.CustomProperties["Score"] - amount;
            Debug.Log(score);
            PhotonNetwork.LocalPlayer.CustomProperties["Score"] = score;
        }

        public void IncreaseScore(int amount)
        {
            int score = (int) PhotonNetwork.LocalPlayer.CustomProperties["Score"] + amount;
            Debug.Log(score);
            PhotonNetwork.LocalPlayer.CustomProperties["Score"] = score;
        }
        
    }
}