﻿using System;
using System.Collections;
using System.Globalization;
using ExitGames.Client.Photon;
using Game;
using GoogleARCore;
using Photon;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Util;

/// <summary>
/// GameUIController - UI Controller to Scene Game
/// </summary>
public class GameUIController : MonoBehaviour
{
    private LocalPlayerController _localPlayerController;

    private SnakeController _snakeController;
    
    private bool _anchorFinishedHosting;

    public Image InterceptIcon;

    public Image ObserveIcon;

    [Header("Snake HUD")]
    public GameObject snakeHUD;
    
    public Text snakeSpeed;

    [Header("Snackbar")]
    public Text snackbar;

    public Text snackbarError;
    
    [Header("Buttons")]
    public Button createAnchorButton;

    public Button playButton;

    [Header("End Game")] public GameObject endGamePanel;

    public Text teamWin;

    [Header("Team Selection")] public GameObject teamSelectionPanel;

    public Button kindSelectionButton, wickedSelectionButton;

    public Text playersTeamKindCount, playersTeamWickedCount;

    [Header("Room Information")] public Text roomNumber;

    public Text playersCount;

    [Header("Player Information")] public GameObject playerInfo;

    public Text myTeam;

    public GameObject myColor;

    public Text myScore;

    private void Awake()
    {
        gameObject.name = "GameUIController";
    }

    void Start()
    {
        _localPlayerController = GameObject.Find("LocalPlayer").GetComponent<LocalPlayerController>();
        _localPlayerController.OnTeamSelected += OnTeamSelected;
        _localPlayerController.OnColorSelected += OnColorSelected;
        _localPlayerController.OnScoreChange += OnScoreChange;
        GameManager.Instance.OnNumPlayersUpdated += OnNumPlayersUpdated;

        _snakeController = GameObject.Find("SnakeController").GetComponent<SnakeController>();
        _snakeController.OnEnergyChanged += OnSnakeEnergyChanged;

        _SetRoomInfo();
        _SetPlayersCount();
        _ChangeVisibility(teamSelectionPanel, true);

        kindSelectionButton.onClick.AddListener(delegate { SelectTeam(Team.Kind); });
        wickedSelectionButton.onClick.AddListener(delegate { SelectTeam(Team.Wicked); });
    }

    private void OnSnakeEnergyChanged(float energy)
    {
        snakeSpeed.text = energy.ToString(CultureInfo.InvariantCulture);
    }

    private void Update()
    {
        _SetButtonPlayersCount();
        kindSelectionButton.enabled = GameManager.Instance.numPlayersTeamKind <= 2;
        wickedSelectionButton.enabled = GameManager.Instance.numPlayersTeamWicked <= 2;

        if (_anchorFinishedHosting)
        {
            if (GameManager.Instance.gameMode != GameMode.Playing)
            {
                _ChangeVisibility(playButton.gameObject, GameManager.Instance.GetNumPlayers() > 1);
            }
        }
    }

    private void OnNumPlayersUpdated()
    {
        _SetPlayersCount();
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += EventReceived;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= EventReceived;
    }

    private void EventReceived(EventData obj)
    {
        if (obj.Code == EventCode.TeamKindWin)
        {
            _SetTeamWin(Team.Kind);
            _ChangeVisibility(endGamePanel, true);
            OnEnterEndGameMode();
        }
        else if (obj.Code == EventCode.TeamWickedWin)
        {
            _SetTeamWin(Team.Wicked);
            _ChangeVisibility(endGamePanel, true);
            OnEnterEndGameMode();
        }
        else if (obj.Code == EventCode.StartGame)
        {
            OnEnterPlayGameMode();
        }
    }

    private void OnEnterPlayGameMode()
    {
        _ChangeVisibility(snakeHUD, true);
        snackbar.text = "Playing";
    }

    private void OnEnterEndGameMode()
    {
        snackbar.text = "End!";
    }

    private void _SetTeamWin(Team team)
    {
        teamWin.text = team + " wins!";
    }

    private void _ChangeVisibility(GameObject gameObject, bool visible)
    {
        gameObject.SetActive(visible);
    }

    private void _SetPlayersCount()
    {
        playersCount.text = "Players: " + GameManager.Instance.GetNumPlayers();
    }

    private void _SetButtonPlayersCount()
    {
        playersTeamKindCount.text = GameManager.Instance.numPlayersTeamKind + "/2";
        playersTeamWickedCount.text = GameManager.Instance.numPlayersTeamWicked + "/2";
    }

    private void _SetRoomInfo()
    {
        roomNumber.text = PhotonManager.Instance.GetRoomNumber();
    }

    /// <summary>
    /// Called when the Anchor is hosted by the master client
    /// </summary>
    /// <param name="success"></param>
    /// <param name="response"></param>
    public void OnAnchorHosted(bool success, string response)
    {
        _anchorFinishedHosting = success;
        snackbar.text = success
            ? "Cloud Anchor successfully hosted!"
            : "Cloud Anchor could not be hosted.∫";
    }

    /// <summary>
    /// Called when the Anchor is resolved in the client
    /// </summary>
    /// <param name="success"></param>
    /// <param name="response"></param>
    public void OnAnchorResolved(bool success, string response)
    {
        snackbar.text = success
            ? "Cloud Anchor successfully resolved!"
            : "Cloud Anchor could not be resolved. Will attempt again. " + response;
    }

    /// <summary>
    /// Called when enters on Hosting mode
    /// </summary>
    public void OnEnterHostingMode()
    {
        snackbar.text = "Find a plane, tap to create a Cloud Anchor.";
    }

    /// <summary>
    /// Called when enters on Resolving mode
    /// </summary>
    public void OnEnterResolvingMode()
    {
        snackbar.text = "Wait for Cloud Anchor to be hosted.";
    }

    /// <summary>
    /// Called when anchor is instantiated and is hosting the cloud anchor
    /// </summary>
    /// <param name="isHost">If isHost is hosting the Cloud Anchor and if not, is resolving it</param>
    public void OnAnchorInstantiated(bool isHost)
    {
        snackbar.text = isHost
            ? "Hosting Cloud Anchor..."
            : "Cloud Anchor added to session! Attempting to resolve anchor...";
    }

    /// <summary>
    /// Called when team is selected
    /// </summary>
    /// <param name="team"></param>
    private void OnTeamSelected(Team team)
    {
        myTeam.text = team.ToString().Substring(0, 1);
        playerInfo.gameObject.SetActive(true);
        _ChangeVisibility(teamSelectionPanel, false);
    }

    private void OnColorSelected(GameColor color)
    {
        if (color == GameColor.Yellow) myTeam.color = ColorUtil.GetColor(GameColor.Text);
        myColor.gameObject.GetComponent<Image>().color = ColorUtil.GetColor(color, 0.75f);
    }

    private void OnScoreChange(int score)
    {
        myScore.text = score.ToString();
    }

    private void SelectTeam(Team team)
    {
        _localPlayerController.SelectTeam(team);
    }

    public void OnPlayButtonClick()
    {
        GameManager.Instance.Play();
        _ChangeVisibility(playButton.gameObject, false);
    }

    public void OnTrackingBoardInstantiated()
    {
        _ChangeVisibility(createAnchorButton.gameObject, true);
    }

    public void OnAnchorButtonClick()
    {
        _ChangeVisibility(createAnchorButton.gameObject, false);
    }

    public void OnReturnToMenuButtonClick()
    {
        PhotonManager.Instance.LeaveRoom();
        SceneManager.LoadScene("Menu");
    }

    public void OnLostTracking()
    {
        switch (Session.LostTrackingReason)
        {
            case LostTrackingReason.InsufficientLight:
                StartCoroutine(ShowError("Too dark. Try moving to a well-lit area."));
                break;
            case LostTrackingReason.InsufficientFeatures:
                StartCoroutine(ShowError("Aim device at a surface with more texture or color."));
                break;
            case LostTrackingReason.ExcessiveMotion:
                StartCoroutine(ShowError("Moving too fast. Slow down."));
                break;
            default:
                StartCoroutine(ShowError("Motion tracking is lost."));
                break;
        }
    }

    IEnumerator ShowError(string errorMessage)
    {
        snackbarError.text = errorMessage;
        yield return new WaitForSeconds(2f);
        snackbarError.text = "";
        yield return null;
    }

    public void SetInterceptIconVisible(bool visible)
    {
        _ChangeVisibility(InterceptIcon.gameObject, visible);
    }

    public void SetObserveIconVisible(bool visible)
    {
        _ChangeVisibility(ObserveIcon.gameObject, visible);
    }
}