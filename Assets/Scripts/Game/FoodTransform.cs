using UnityEngine;

namespace Game
{
    /// <summary>
    /// Apply motion to the Food Object
    /// </summary>
    public class FoodTransform : MonoBehaviour
    {
        public float speed = 20f;
        
        // Object rotates forever
        private void Update()
        {
            transform.Rotate(Vector3.down, Time.deltaTime * speed * 5);
        }
    }
}