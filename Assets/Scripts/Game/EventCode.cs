namespace Game
{
    public abstract class EventCode
    {
        public const int StartGame = 1;
        public const int EndGame = 2;
        public const int Punish = 3;
        public const int Pollute = 4;
        public const int TeamWickedWin = 5;
        public const int TeamKindWin = 6;
        public const int IncreaseSpeed = 7;
        public const int ReduceSpeed = 8;
    }
}