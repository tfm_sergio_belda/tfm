using System;
using ExitGames.Client.Photon;
using Photon.Pun;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// SnakeController 
    /// </summary>
    public class SnakeController : MonoBehaviour
    {
        public GameObject snakePrefab;

        private GameObject _snakeInstance;
        
        public int energy;

        public delegate void EnergyChangedEventHandler(float energy);
        
        public PhotonView photonView;

        public event EnergyChangedEventHandler OnEnergyChanged;
        
        private void Awake()
        {
            gameObject.name = "SnakeController";
        }
        
        public void OnEnable()
        {
            PhotonNetwork.NetworkingClient.EventReceived += EventReceived;
        }

        public void OnDisable()
        {
            PhotonNetwork.NetworkingClient.EventReceived -= EventReceived;
        }
        
        private void EventReceived(EventData obj)
        {
            if (obj.Code == EventCode.StartGame)
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    _InstantiateSnake();
                }
            } else if (obj.Code == EventCode.EndGame)
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    PhotonNetwork.Destroy(_snakeInstance);
                }
            } else if (obj.Code == EventCode.IncreaseSpeed)
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    IncreaseEnergy();
                }
            } else if (obj.Code == EventCode.ReduceSpeed)
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    DecreaseEnergy();
                }
            }
        }

        private void IncreaseEnergy()
        {
            if (energy == 100) return;
            energy += 25;
            photonView.RPC("RPC_UpdateEnergy", RpcTarget.AllBuffered, energy);
            OnEnergyChanged?.Invoke(energy);
            
            UpdateSpeed();
        }
        
        private void DecreaseEnergy()
        {
            if (energy == 0) return;
            energy -= 25;
            photonView.RPC("RPC_UpdateEnergy", RpcTarget.AllBuffered, energy);
            OnEnergyChanged?.Invoke(energy);

            UpdateSpeed();
        }
        
        [PunRPC]
        private void RPC_UpdateEnergy(int energy)
        {
            this.energy = energy;
            OnEnergyChanged?.Invoke(energy);
        }

        private void UpdateSpeed()
        {
            if (_snakeInstance != null)
            {
                _snakeInstance.GetComponent<SnakeTransform>().speed = (float) (energy * 0.003);
            }
        }

        /// <summary>
        /// Instantiates the Snake prefab
        /// </summary>
        private void _InstantiateSnake()
        {
            var position = Vector3.zero;
            position.y += 0.06f;
            _snakeInstance = PhotonNetwork.Instantiate(snakePrefab.name, position, Quaternion.identity);
            GetComponent<Slithering>().Head = _snakeInstance.transform;
            _snakeInstance.AddComponent<FoodConsumer>();
        }
    }
}