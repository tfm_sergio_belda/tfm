namespace Game
{
    public enum GameColor
    {
        None,
        Yellow,
        Blue,
        Green,
        Red,
        RedError,
        Text
    }
}