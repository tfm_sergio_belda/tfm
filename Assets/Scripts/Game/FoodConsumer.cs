using System;
using UnityEngine;

namespace Game
{
    public class FoodConsumer : MonoBehaviour
    {
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("food"))
            {
                collision.gameObject.SetActive(false);
                Slithering s = GetComponentInParent<Slithering>();

                if (s != null)
                {
                    s.AddBodyPart();
                }
            }
        }
    }
}