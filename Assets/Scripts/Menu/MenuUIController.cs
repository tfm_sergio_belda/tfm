﻿using System.Collections.Generic;
using Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;

namespace Menu
{
    public class MenuUIController : MonoBehaviourPunCallbacks
    {
        /// <summary>
        /// Create Room components
        /// </summary>
        [Header("Create Room")] public Button createRoomButton;

        /// <summary>
        /// Shows the current rooms list
        /// </summary>
        [Header("Room List Panel")] public GameObject roomListPanel;

        public GameObject roomListContent;
        public GameObject roomListEntryPrefab;

        

        private Dictionary<string, RoomInfo> cachedRoomList;
        private Dictionary<string, GameObject> roomListEntries;

        private void Awake()
        {
            createRoomButton.enabled = false;

            cachedRoomList = new Dictionary<string, RoomInfo>();
            roomListEntries = new Dictionary<string, GameObject>();
        }

        public void OnCreateRoomButtonClick()
        {
            PhotonManager.Instance.JoinOrCreateRoom();
        }

        private void ClearRoomListView()
        {
            foreach (GameObject entry in roomListEntries.Values)
            {
                Destroy(entry.gameObject);
            }

            roomListEntries.Clear();
        }

        public override void OnLeftLobby()
        {
            cachedRoomList.Clear();

            ClearRoomListView();
        }

        public override void OnRoomListUpdate(List<RoomInfo> roomList)
        {
            ClearRoomListView();

            UpdateCachedRoomList(roomList);
            UpdateRoomListView();
        }


        private void UpdateCachedRoomList(List<RoomInfo> roomList)
        {
            foreach (RoomInfo info in roomList)
            {
                // Remove room from cached room list if it got closed, became invisible or was marked as removed
                if (!info.IsOpen || !info.IsVisible || info.RemovedFromList)
                {
                    if (cachedRoomList.ContainsKey(info.Name))
                    {
                        cachedRoomList.Remove(info.Name);
                    }

                    continue;
                }

                // Update cached room info
                if (cachedRoomList.ContainsKey(info.Name))
                {
                    cachedRoomList[info.Name] = info;
                }
                // Add new room info to cache
                else
                {
                    cachedRoomList.Add(info.Name, info);
                }
            }
        }

        private void UpdateRoomListView()
        {
            foreach (RoomInfo info in cachedRoomList.Values)
            {
                GameObject entry = Instantiate(roomListEntryPrefab, roomListContent.transform, true);
                entry.transform.localScale = Vector3.one;
                entry.GetComponent<RoomListEntry>().Initialize(info.Name, (byte) info.PlayerCount, info.MaxPlayers);

                roomListEntries.Add(info.Name, entry);
            }
        }
        
        public override void OnJoinedLobby()
        {
            createRoomButton.enabled = true;
        }

        public override void OnConnected()
        {
            createRoomButton.enabled = true;
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            createRoomButton.enabled = false;
        }
    }
}