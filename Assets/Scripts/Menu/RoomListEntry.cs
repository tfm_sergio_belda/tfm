using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

namespace Menu
{
    public class RoomListEntry : MonoBehaviour
    {
        public Text roomNameText;
        public Text roomPlayersText;
        public Button joinRoomButton;

        private string _roomName;

        public void Start()
        {
            joinRoomButton.onClick.AddListener(() =>
            {
                if (PhotonNetwork.InLobby)
                {
                    PhotonNetwork.LeaveLobby();
                }

                PhotonNetwork.JoinRoom(_roomName);
            });
        }

        public void Initialize(string name, byte currentPlayers, byte maxPlayers)
        {
            _roomName = name;
            roomNameText.text = name;
            roomPlayersText.text = currentPlayers + " / " + maxPlayers;
        }
    }
}