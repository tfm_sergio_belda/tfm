﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Photon
 {
     public class PhotonManager : MonoBehaviourPunCallbacks
     {
         public static PhotonManager Instance;
    
         public GameObject player;

         private void Awake()
         {
             if (Instance != null)
             {
                 DestroyImmediate(gameObject);
                 return;
             }
             
             DontDestroyOnLoad(gameObject);
             Instance = this;
             PhotonNetwork.AutomaticallySyncScene = true;
             SceneManager.sceneLoaded += OnSceneFinishedLoading;
         }

         public void JoinOrCreateRoom()
         {
             var roomOptions = new RoomOptions {MaxPlayers = 4};
             var roomName = UnityEngine.Random.Range(0, 1000).ToString();
             PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, TypedLobby.Default);
         }

         public void ChangeRoomVisibility(bool visible)
         {
             PhotonNetwork.CurrentRoom.IsVisible = visible;
         }

         public void LeaveRoom()
         {
             if (PhotonNetwork.InRoom) PhotonNetwork.LeaveRoom();
         }

         public string GetRoomNumber()
         {
             if (PhotonNetwork.InRoom) return "Room: " + PhotonNetwork.CurrentRoom.Name;
             return "";
         }

         public int GetRoomPlayersCount()
         {
             return PhotonNetwork.InRoom ? PhotonNetwork.CurrentRoom.PlayerCount : 0;
         }

         public override void OnJoinedRoom()
         {
             if (PhotonNetwork.IsMasterClient)
             {
                 PhotonNetwork.LoadLevel("Game");
             }
         }

         private void OnSceneFinishedLoading(Scene scene, LoadSceneMode loadSceneMode)
         {
             if (scene.name != "Game") return;
             SpawnPlayer();
         }

         private void SpawnPlayer()
         {
             PhotonNetwork.Instantiate(player.name, player.transform.position, player.transform.rotation, 0);
         }
         
         
     }
 }