﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

/// <summary>
/// PhotonConnection - Establishes the connection with the Photon services when system is launched
/// </summary>
public class PhotonConnection : MonoBehaviourPunCallbacks
{
    public string versionName = "0.1";

    /// <summary>
    /// Connects with PhotonNetwork
    /// </summary>
    private void Awake()
    {
        PhotonNetwork.GameVersion = versionName;
        PhotonNetwork.ConnectUsingSettings();
        Debug.Log("Connecting to photon...");
    }

    /// <summary>
    /// On connected to master
    /// </summary>
    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby(TypedLobby.Default);
        Debug.Log("We are connected to master");
    }

    /// <summary>
    /// Joined to Lobby
    /// </summary>
    public override void OnJoinedLobby()
    {
        Debug.Log("Joined Lobby");
    }

    /// <summary>
    /// Disconnected from PhotonNetwork
    /// </summary>
    /// <param name="cause">Disconnection cause</param>
    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("Disconnect from photon services");
    }
}