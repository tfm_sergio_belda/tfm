﻿using System.Collections;
using System.Collections.Generic;
using Game;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;
using Util;

public class PhotonConnectionStateController : MonoBehaviourPunCallbacks
{
    /// <summary>
    /// Shows if it's connected or disconnected
    /// </summary>
    public Text stateLabel;

    /// <summary>
    /// Indicator that shows the state through the color red or green
    /// </summary>
    public GameObject stateColor;

    /// <summary>
    /// Joined to Lobby
    /// </summary>
    public override void OnJoinedLobby()
    {
        stateLabel.text = "Connected";
        stateLabel.color = ColorUtil.GetColor(GameColor.Green);
    }

    public override void OnConnected()
    {
        stateLabel.text = "Connected";
        stateLabel.color = ColorUtil.GetColor(GameColor.Green);
        stateColor.gameObject.GetComponent<Image>().color = ColorUtil.GetColor(GameColor.Green);
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        stateLabel.text = "Disconnected";
        stateLabel.color = ColorUtil.GetColor(GameColor.RedError);
        stateColor.gameObject.GetComponent<Image>().color = ColorUtil.GetColor(GameColor.RedError);
    }
}
