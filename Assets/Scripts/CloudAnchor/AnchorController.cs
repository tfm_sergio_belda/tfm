﻿using Game;
using Photon.Pun;

namespace CloudAnchor
{
    using GoogleARCore;
    using GoogleARCore.CrossPlatform;
    using UnityEngine;

    /// <summary>
    /// A Controller for the Anchor object that handles hosting and resolving the Cloud Anchor.
    /// </summary>
    public class AnchorController : MonoBehaviourPunCallbacks
    {
        public PhotonView photonView;

        /// <summary>
        /// The Cloud Anchor ID that will be used to host and resolve the Cloud Anchor. This
        /// variable will be synchronized over all clients.
        /// </summary>
        private string _mCloudAnchorId = string.Empty;

        /// <summary>
        /// Indicates whether this script is running in the Host.
        /// </summary>
        private bool _mIsHost;

        /// <summary>
        /// Indicates whether an attempt to resolve the Cloud Anchor should be made.
        /// </summary>
        private bool _mShouldResolve;

        /// <summary>
        /// The Cloud Anchors example controller.
        /// </summary>
        private GameSceneController _mGameSceneController;

        /// <summary>
        /// The Unity Start() method.
        /// </summary>
        public void Start()
        {
            photonView = GetComponent<PhotonView>();
            _mGameSceneController = GameObject.Find("GameSceneController").GetComponent<GameSceneController>();

            if (!photonView.IsMine)
            {
                OnStartClient();
            }
        }

        /// <summary>
        /// The Unity OnStartClient() method.
        /// </summary>
        private void OnStartClient()
        {
            if (_mCloudAnchorId != string.Empty)
            {
                _mShouldResolve = true;
            }
        }

        /// <summary>
        /// The Unity Update() method.
        /// </summary>
        public void Update()
        {
            if (_mShouldResolve)
            {
                _ResolveAnchorFromId(_mCloudAnchorId);
            }
        }

        /// <summary>
        /// Command run on the server to set the Cloud Anchor Id.
        /// </summary>
        /// <param name="cloudAnchorId">The new Cloud Anchor Id.</param>
        [PunRPC]
        public void PunRpcSetCloudAnchorId(string cloudAnchorId)
        {
            _mCloudAnchorId = cloudAnchorId;
        }

        /// <summary>
        /// Gets the Cloud Anchor Id.
        /// </summary>
        /// <returns>The Cloud Anchor Id.</returns>
        public string GetCloudAnchorId()
        {
            return _mCloudAnchorId;
        }

        /// <summary>
        /// Hosts the user placed cloud anchor and associates the resulting Id with this object.
        /// </summary>
        /// <param name="lastPlacedAnchor">The last placed anchor.</param>
        public void HostLastPlacedAnchor(Component lastPlacedAnchor)
        {
            _mIsHost = true;

            var anchor = (Anchor) lastPlacedAnchor;

            XPSession.CreateCloudAnchor(anchor).ThenAction(result =>
            {
                if (result.Response != CloudServiceResponse.Success)
                {
                    Debug.Log(string.Format("Failed to host Cloud Anchor: {0}", result.Response));

                    _mGameSceneController.OnAnchorHosted(
                        false, result.Response.ToString());
                    return;
                }

                Debug.Log(string.Format(
                    "Cloud Anchor {0} was created and saved.", result.Anchor.CloudId));

                photonView.RPC("PunRpcSetCloudAnchorId", RpcTarget.AllBufferedViaServer, result.Anchor.CloudId);

                _mGameSceneController.OnAnchorHosted(true, result.Response.ToString());
            });
        }

        /// <summary>
        /// Resolves an anchor id and instantiates an Anchor prefab on it.
        /// </summary>
        /// <param name="cloudAnchorId">Cloud anchor id to be resolved.</param>
        private void _ResolveAnchorFromId(string cloudAnchorId)
        {
            _mGameSceneController.OnAnchorInstantiated(false);

            // If device is not tracking, let's wait to try to resolve the anchor.
            if (Session.Status != SessionStatus.Tracking)
            {
                return;
            }

            _mShouldResolve = false;

            XPSession.ResolveCloudAnchor(cloudAnchorId).ThenAction(result =>
            {
                if (result.Response != CloudServiceResponse.Success)
                {
                    Debug.LogError(string.Format(
                        "Client could not resolve Cloud Anchor {0}: {1}",
                        cloudAnchorId, result.Response));

                    _mGameSceneController.OnAnchorResolved(
                        false, result.Response.ToString());
                    _mShouldResolve = true;
                    return;
                }

                Debug.Log(string.Format(
                    "Client successfully resolved Cloud Anchor {0}.",
                    cloudAnchorId));

                _mGameSceneController.OnAnchorResolved(
                    true, result.Response.ToString());
                _OnResolved(result.Anchor.transform);
            });
        }

        /// <summary>
        /// Callback invoked once the Cloud Anchor is resolved.
        /// </summary>
        /// <param name="anchorTransform">Transform of the resolved Cloud Anchor.</param>
        private void _OnResolved(Transform anchorTransform)
        {
            _mGameSceneController.SetWorldOrigin(anchorTransform);
        }

        /// <summary>
        /// Callback invoked once the Cloud Anchor Id changes.
        /// </summary>
        /// <param name="newId">New identifier.</param>
        private void _OnChangeId(string newId)
        {
            if (!_mIsHost && newId != string.Empty)
            {
                _mCloudAnchorId = newId;
                _mShouldResolve = true;
            }
        }
    }
}