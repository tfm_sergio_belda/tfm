using Game;
using OpenCvSharp;
using UnityEngine;

namespace Util
{
    public abstract class ColorUtil
    {
        private const uint Blue = 0xFF1976d2;
        private const uint Red = 0xFFd32f2f;
        private const uint RedError = 0xFF8e0000;
        private const uint Green = 0xFF00766c;
        private const uint Yellow = 0xFFFFEB3B;
        private const uint Gray = 0xFF90A4AE;
        private const uint Text = 0xFF636363;

        public static Color GetColor(uint color)
        {
            var a = (byte)(color >> 24);
            var r = (byte)(color >> 16);
            var g = (byte)(color >> 8);
            var b = (byte)(color >> 0);
            return new Color(r/255.0f, g/255.0f, b/255.0f, a/255.0f);
        }
        
        public static Color GetColor(uint color, float alpha)
        {
            var r = (byte)(color >> 16);
            var g = (byte)(color >> 8);
            var b = (byte)(color >> 0);
            return new Color(r/255.0f, g/255.0f, b/255.0f, alpha);
        }

        public static Color GetColor(GameColor gameColor)
        {
            switch (gameColor)
            {
                case GameColor.Red : return GetColor(Red);
                case GameColor.Blue : return GetColor(Blue);
                case GameColor.Green : return GetColor(Green);
                case GameColor.Yellow : return GetColor(Yellow);
                case GameColor.RedError : return GetColor(RedError);
                case GameColor.Text : return GetColor(Text);
                default: return GetColor(Gray);
            }
        }

        public static Color GetColor(GameColor gameColor, float alpha)
        {
            switch (gameColor)
            {
                case GameColor.Red : return GetColor(Red, alpha);
                case GameColor.Blue : return GetColor(Blue, alpha);
                case GameColor.Green : return GetColor(Green, alpha);
                case GameColor.Yellow : return GetColor(Yellow, alpha);
                case GameColor.RedError : return GetColor(RedError, alpha);
                case GameColor.Text : return GetColor(Text, alpha);
                default: return GetColor(Gray, alpha);
            }
        }

        public static Scalar GetHSVLowerBound(GameColor gameColor)
        {
            switch (gameColor)
            {
                case GameColor.Red : return new Scalar(0, 76, 63);
                case GameColor.Blue : return new Scalar(110, 50, 50);
                case GameColor.Green : return new Scalar(34, 50, 50);
                case GameColor.Yellow : return new Scalar(20, 124, 123);
                default: return new Scalar(0, 0, 50);
            }
        }

        public static Scalar GetHSVUpperBound(GameColor gameColor)
        {
            switch (gameColor)
            {
                case GameColor.Red : return new Scalar(5, 255, 255);
                case GameColor.Blue : return new Scalar(130, 255, 255);
                case GameColor.Green : return new Scalar(80, 220, 200);
                case GameColor.Yellow : return new Scalar(30, 256, 256);
                default: return new Scalar(0, 0, 50);
            }
        }
    }
}