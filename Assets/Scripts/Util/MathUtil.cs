using OpenCvSharp;

namespace Util
{
    public static class MathUtil
    {
        public static bool PointInsideTriangle(Point2f p, Point2f a, Point2f b, Point2f c)
        {
            var alfa = ((b.Y - c.Y) * (p.X - c.X) + (c.X - b.X) * (p.Y - c.Y)) / ((b.Y - c.Y) * (a.X - c.X) + (c.X - b.X) * (a.Y - c.Y));
            var beta = ((c.Y - a.Y) * (p.X - c.X) + (a.X - c.X) * (p.Y - c.Y)) / ((b.Y - c.Y) * (a.X - c.X) + (c.X - b.X) * (a.Y - c.Y));
            var gamma = 1.0f - alfa - beta;

            return alfa >= 0 && alfa <= 1 && beta >= 0 && beta <= 1 && gamma >= 0 && gamma <= 1;
        }
    }
}