namespace ComputerVision
{
    public abstract class Marker
    {
        public static bool GetMarkerHealthiness(int number)
        {
            switch (number)
            {
                case 5 : return true;
                case 23 : return false;
                default : return false;
            }
        }
    }
}