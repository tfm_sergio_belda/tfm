﻿using System;
using System.Collections;
using System.IO;
using ExitGames.Client.Photon;
using Game;
using GoogleARCore.Examples.ComputerVision;
using OpenCvSharp;
using OpenCvSharp.Aruco;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;
using Util;
using EventCode = Game.EventCode;
using Point = OpenCvSharp.Point;
using Size = OpenCvSharp.Size;

namespace ComputerVision
{
    public class ComputerVisionController : MonoBehaviour
    {
        private LocalPlayerController _localPlayerController;
        private TextureReader _textureReader;
        private Texture2D _texture, _outputTexture, _outputTexture2;
        private int _imageWidth;
        private int _imageHeight;
        private byte[] _image;

        public GameUIController gameUiController;

        public RawImage markerImage, openCVImage;

        private const GameColor YELLOW = GameColor.Yellow;
        private const GameColor RED = GameColor.Red;
        private const GameColor BLUE = GameColor.Blue;
        private const GameColor GREEN = GameColor.Green;

        private bool _observe, _intercept;

        private void Awake()
        {
            _textureReader = GetComponent<TextureReader>();
        }

        public void OnEnable()
        {
            PhotonNetwork.NetworkingClient.EventReceived += EventReceived;
        }

        public void OnDisable()
        {
            PhotonNetwork.NetworkingClient.EventReceived -= EventReceived;
        }

        // Start is called before the first frame update
        private void Start()
        {
            _localPlayerController = GameObject.Find("LocalPlayer").GetComponent<LocalPlayerController>();
            StartCoroutine(ImageProcessing());
        }

        // Update is called once per frame
        private void Update()
        {
            gameUiController.SetInterceptIconVisible(_intercept);
            gameUiController.SetObserveIconVisible(_observe);
        }

        private void EventReceived(EventData obj)
        {
            if (obj.Code == EventCode.StartGame)
            {
                _textureReader.OnImageAvailableCallback += OnImageAvailable;
                _observe = true;
                _intercept = true;
            }
            else if (obj.Code == EventCode.EndGame)
            {
                _textureReader.OnImageAvailableCallback -= OnImageAvailable;
            }
        }

        private void OnImageAvailable(TextureReaderApi.ImageFormatType format, int width, int height,
            IntPtr pixelBuffer,
            int bufferSize)
        {
            if (_texture == null || _image == null || _imageWidth != width || _imageHeight != height)
            {
                _texture = new Texture2D(width, height, TextureFormat.RGBA32, false, false);
                _image = new byte[width * height * 4];
                _imageWidth = width;
                _imageHeight = height;
            }

            System.Runtime.InteropServices.Marshal.Copy(pixelBuffer, _image, 0, bufferSize);
        }

        IEnumerator ImageProcessing()
        {
            while (true)
            {
                try
                {
                    if (_image != null)
                    {
                        // Update the rendering texture with the sampled image.
                        _texture.LoadRawTextureData(_image);
                        _texture.Apply();

                        Point2f[][] markers;
                        int[] ids;
                        Point2f[][] rejectedImgPoints;

                        // Create default parameters for detection
                        var detectorParameters = DetectorParameters.Create();

                        // Dictionary holds set of all available markers
                        var dictionary = CvAruco.GetPredefinedDictionary(PredefinedDictionaryName.Dict6X6_250);

                        // Transform to Mat
                        var mat = OpenCvSharp.Unity.TextureToMat(_texture);
                        Cv2.Rotate(mat, mat, RotateFlags.Rotate90Clockwise);
                        Cv2.Flip(mat, mat, FlipMode.Y);

                        // Gray Mat and Detect Markers
                        var grayMat = mat.CvtColor(ColorConversionCodes.RGBA2GRAY);
                        CvAruco.DetectMarkers(grayMat, dictionary, out markers, out ids, detectorParameters,
                            out rejectedImgPoints);

                        if (markers.Length > 0)
                        {
                            try
                            {
                                for (int j = 0; j < markers.Length; j++)
                                {
                                    var markerPoints = markers[j];
                                    if (markerPoints.Length == 4)
                                    {
                                        var points = new Point[1][];

                                        points[0] = new Point[]
                                            {markerPoints[0], markerPoints[1], markerPoints[2], markerPoints[3]};
                                        var m = Cv2.Moments(points[0]);
                                        Point centerA = new Point(m.M10 / m.M00, m.M01 / m.M00);

                                        const int factor = 4;
                                        points[0] = new Point[]
                                        {
                                            markerPoints[0] * factor, markerPoints[1] * factor,
                                            markerPoints[2] * factor,
                                            markerPoints[3] * factor
                                        };
                                        m = Cv2.Moments(points[0]);
                                        Point centerB = new Point(m.M10 / m.M00, m.M01 / m.M00);

                                        int distanceX = Math.Abs(centerA.X - centerB.X);
                                        int distanceY = Math.Abs(centerA.Y - centerB.Y);

                                        for (int i = 0; i < points[0].Length; i++)
                                        {
                                            points[0][i].X -= distanceX;
                                            points[0][i].Y -= distanceY;
                                        }

                                        // Draw Area
                                        Cv2.Polylines(mat, points, true, new Scalar(255, 255, 50), 2);

                                        // Get texture inside area
                                        var src = new Point2f[4];
                                        var dst = new Point2f[4];

                                        src[0] = points[0][0];
                                        src[1] = points[0][1];
                                        src[2] = points[0][2];
                                        src[3] = points[0][3];

                                        dst[0] = new Point2f(0, 0);
                                        dst[1] = new Point2f(0, 255);
                                        dst[2] = new Point2f(255, 255);
                                        dst[3] = new Point2f(255, 0);

                                        var outputMat = new Mat();
                                        Mat transform = Cv2.GetPerspectiveTransform(src, dst);
                                        Cv2.WarpPerspective(mat, outputMat, transform, new Size(255, 255));

                                        foreach (var color in GameManager.Instance.GetCurrentGameColors())
                                        {
                                            if (DetectBlobs(outputMat, color))
                                            {
                                                var healthyMarker = Marker.GetMarkerHealthiness(ids[j]);
                                                RaiseEvents(healthyMarker, color);
                                            }
                                        }

                                        if (_outputTexture2 != null)
                                        {
                                            DestroyImmediate(_outputTexture2);
                                        }

                                        if (outputMat != null)
                                        {
                                            _outputTexture2 = OpenCvSharp.Unity.MatToTexture(outputMat);
                                        }

                                        markerImage.texture = _outputTexture2;
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                gameUiController.snackbar.text = e.ToString();
                            }
                        }

                        if (_outputTexture != null)
                        {
                            DestroyImmediate(_outputTexture);
                        }

                        _outputTexture = OpenCvSharp.Unity.MatToTexture(mat);

                        openCVImage.texture = _outputTexture;
                    }
                }
                catch (Exception e)
                {
                    gameUiController.snackbar.text = e.ToString();
                }

                yield return new WaitForSeconds(0.5f);
            }
        }

        private void RaiseEvents(bool healthyMarker, GameColor blobColor)
        {
            if (_localPlayerController.myColor == blobColor)
            {
                if (!_observe) return;
                if (_localPlayerController.myTeam == Team.Kind)
                {
                    if (healthyMarker)
                    {
                        _localPlayerController.IncreaseScore(10);
                        PhotonNetwork.RaiseEvent(EventCode.IncreaseSpeed, null,
                            new RaiseEventOptions {Receivers = ReceiverGroup.MasterClient},
                            SendOptions.SendUnreliable);
                        
                        gameUiController.snackbar.text = "Observed blob " + blobColor;
                    }
                    else
                    {
                        _localPlayerController.ReduceScore(10);
                        PhotonNetwork.RaiseEvent(EventCode.ReduceSpeed, null,
                            new RaiseEventOptions {Receivers = ReceiverGroup.MasterClient},
                            SendOptions.SendUnreliable);
                        
                        gameUiController.snackbar.text = "Observed blob " + blobColor;
                    }
                }
                else
                {
                    if (!healthyMarker)
                    {
                        _localPlayerController.IncreaseScore(10);
                        PhotonNetwork.RaiseEvent(EventCode.ReduceSpeed, null,
                            new RaiseEventOptions {Receivers = ReceiverGroup.MasterClient},
                            SendOptions.SendUnreliable);
                        
                        gameUiController.snackbar.text = "Observed blob " + blobColor;

                    }
                    else
                    {
                        _localPlayerController.ReduceScore(10);
                        PhotonNetwork.RaiseEvent(EventCode.IncreaseSpeed, null,
                            new RaiseEventOptions {Receivers = ReceiverGroup.MasterClient},
                            SendOptions.SendUnreliable);
                        
                        gameUiController.snackbar.text = "Observed blob " + blobColor;

                    }
                }
                _observe = false;
                StartCoroutine(ObserveTimer());
            }
            else
            {
                if (!_intercept) return;
                object[] content = { blobColor };
                if (_localPlayerController.myTeam == Team.Kind)
                {
                    if (!healthyMarker)
                    {
                        PhotonNetwork.RaiseEvent(EventCode.Punish, content,
                            new RaiseEventOptions {Receivers = ReceiverGroup.All},
                            SendOptions.SendUnreliable);
                        PhotonNetwork.RaiseEvent(EventCode.IncreaseSpeed, null,
                            new RaiseEventOptions {Receivers = ReceiverGroup.MasterClient},
                            SendOptions.SendUnreliable);
                    }
                }
                else
                {
                    if (healthyMarker)
                    {
                        PhotonNetwork.RaiseEvent(EventCode.Pollute, content,
                            new RaiseEventOptions {Receivers = ReceiverGroup.All},
                            SendOptions.SendUnreliable);
                        PhotonNetwork.RaiseEvent(EventCode.ReduceSpeed, null,
                            new RaiseEventOptions {Receivers = ReceiverGroup.MasterClient},
                            SendOptions.SendUnreliable);
                    }
                }
                _intercept = false;
                StartCoroutine(InterceptTimer());
            }
        }

        private bool DetectBlobs(Mat outputMat, GameColor blobColor)
        {
            var hsv = outputMat.CvtColor(ColorConversionCodes.BGR2HSV);
            var bin = hsv.InRange(ColorUtil.GetHSVLowerBound(blobColor), ColorUtil.GetHSVUpperBound(blobColor));

            var detectorParams = new SimpleBlobDetector.Params
            {
                FilterByCircularity = false,
                FilterByInertia = false,
                FilterByArea = true,
                MinArea = 30f,
                MaxArea = 1000f,
                FilterByColor = false,
                FilterByConvexity = false
            };
            var simpleBlobDetector = SimpleBlobDetector.Create(detectorParams);

            var keypoints = simpleBlobDetector.Detect(bin);
            if (keypoints.Length <= 0) return false;
            Cv2.DrawKeypoints(
                outputMat, //input image
                keypoints: keypoints,
                outImage: outputMat, //output image
                color: Scalar.Red,
                flags: DrawMatchesFlags.DrawRichKeypoints
            );
            return true;
        }

        private IEnumerator ObserveTimer()
        {
            var timeLeft = 5;
            while (timeLeft != 0)
            {
                yield return new WaitForSeconds(1f);
                timeLeft--;
            }

            _observe = true;
        }

        private IEnumerator InterceptTimer()
        {
            var timeLeft = 5;
            while (timeLeft != 0)
            {
                yield return new WaitForSeconds(1f);
                timeLeft--;
            }

            _intercept = true;
        }
    }
}